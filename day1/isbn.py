# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
    isbn_check = int(isbn[-1])
    isbn_numbers = isbn[4:-2]
    isbn_main = [int(d) for d in isbn_numbers]

    # todo finish this function
    return isbn_main, isbn_check

def is_isbn_valid(isbn):
    isbn_main, isbn_check = parse_isbn(isbn)
    checksum = isbn_check

    for n, d in enumerate(isbn_main):
        checksum += n * (10 - d)

    return checksum % 11 == 0

# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')

