# return v if in between minv, maxv. Otherwise return minv or maxv, whichever is closer to v
def clamp(v, minv, maxv):
    if minv > v:
        return minv
    elif maxv < v:
        return maxv
    return v

# given a number of days return one of these human readable
# strings:  less than a day old, n days old, more than a weak old
def human_time(days):
    if days < 1:
        return 'less than a day old'
    if days < 7:
        return str(days) + ' days old'
    return 'more than a week old'

def test_clamp():
    assert clamp(2, 0, 10) == 2
    assert clamp(2, 5, 10) == 5
    assert clamp(13, 5, 8) == 8

def test_human_time():
    assert human_time(0.1) == 'less than a day old'
    assert human_time(2) == '2 days old'
    assert human_time(22) == 'more than a week old'
