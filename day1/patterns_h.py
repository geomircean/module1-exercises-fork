# exercitiu
# o . . . o
# o o . o o
# o o o o o
# o o . o o
# o . . . o
def pattern2():
    N = 5
    for i in range(N):
        for j in range(N):
            if i == j or i + j == N - 1:
                print 'o',
            elif i + j > N - 1 and i < j:
                print 'o',
            elif i + j < N - 1 and j < i:
                print 'o',
            else:
                print '.',
        print


# Let there be a ring centered in the plane at 0, 0
# The ring has the small radius r_m and the big radius r_M.
# Write a function that tests if the point at coordinates x, y is in the ring
def in_ring(x, y, r_m, r_M):
    pass

# using in_ring and rescale from lab1_h.py draw a ring
def ring():
    pass

# . . . . o . . . .
# . . . o o o . . .
# . . o o o o o . .
# . o o o o o o o .
# o o o o o o o o o
# . o o o o o o o .
# . . o o o o o . .
# . . . o o o . . .
# . . . . o . . . .
def pattern3():
    N = 9
    lp = 1
    for i in range(N):
        for j in range(N):
            dist = abs(i - N / 2) ** lp + abs(j - N / 2) ** lp
            dist = dist ** (1.0 / lp)
            if dist <= N / 2:
                print 'o',
            else:
                print '.',
        print

# --- py.test code below ---

def test_pattern2(capsys):
    pattern2()
    out, err = capsys.readouterr()
    assert out == '''\
o . . . o
o o . o o
o o o o o
o o . o o
o . . . o
'''


def voronoi(centers, N=24):
    """
    prints a voronoi space partitioning.
    centers is a list of triples (x, y, name), one describing each center
    N is the grid size
    """
    for i in xrange(N):
        for j in xrange(N):
            min_dist = 1000
            name = ''
            for ci, cj, cn in centers:
                if (i, j) == (ci, cj):
                    name = cn.upper()
                    break
                dist = (i - ci)**2 + (j - cj)**2
                dist = dist**0.5
                if dist < min_dist:
                    min_dist = dist
                    name = cn
            print name,
        print

def run_voronoi():
    voronoi([(2, 3, 'a'), (5, 2, 't'), (4, 4, 'o'),
             (10, 22, 'x'), (20, 7, 'u')],
            N=24)
