import sys


def _parse_line(line):
    """
    >>> _parse_line('ana are')
    (['ana', 'are'], {})
    >>> _parse_line('ana --are 4')
    (['ana'], {'are': '4'})
    >>> _parse_line('--are 4 ana')
    Traceback (most recent call last):
    ...
    ValueError: positional arg after keyword arg
    >>> _parse_line('--34 3')
    Traceback (most recent call last):
    ...
    ValueError: keyword arg must start with letter
    >>> _parse_line('ana --are')
    Traceback (most recent call last):
    ...
    ValueError: missing keyword arg value
    >>>> __parse_line('--are 45')
    """

    all_args = line.split()
    dash_pos = line.find('--')
    returned_arguments = {}

    if dash_pos == -1:
        dash_pos = len(line)

    fn_words = line[:dash_pos].split()
    fn_arguments = line[dash_pos:].split()

    for i in xrange(0, len(fn_arguments), 2):
        kv = fn_arguments[i:i+2]
        if not kv[0].startswith('--'):
            raise ValueError('positional arg after keyword arg')
        if not kv[0][2:].isalpha:
            raise ValueError('keyword arg must start with letter')
        if not kv[1]:
            raise ValueError('missing keyword arg value')
        key, val = kv
        returned_arguments[key[2:]] = val

    return fn_words, returned_arguments


def ana(x):
    print x, 'from ana'


def command(func):
    _registry[func.__name__] = func
    return func

_registry = {'ana': ana}


def main():
    """
    usage:
    script.py function_name arg1 arg2 ... --kwarg1 value1 --kwarg2 value2 ...
    """
    cmdline = ' '.join(sys.argv[1:])
    args, kwargs = _parse_line(cmdline)
    if not args:
        sys.stderr.write('no command given')
    func_name = args[0]
    args = args[1:]

    if func_name not in _registry:
        sys.stderr.write('unknown command %s' %func_name)
        return

    _registry[func_name](*args, **kwargs)


