import json
from codecs import open


class SimpleDb(object):
    def __init__(self, path):
        self.path = path
        try:
            with open(path) as f:
                self.db = json.load(f)
        except IOError:
            self.db = {'catalogs': {}}

        if 'catalogs' not in self.db:
            raise ValueError('bad db')
        self.sane()

    @property
    def catalogs(self):
        return self.db['catalogs']

    def sane(self):
        if not self.db:
            return

        if 'catalogs' not in self.db:
            raise ValueError('bad db')

        if type(self.catalogs) is not dict:
            raise ValueError('bad db')
        # for k, w in self.catalogs.iteritems():
        # TODO: moar whatever sane stuff needed

    def get_catalog(self, cat):
        if cat not in self.catalogs:
            self.catalogs[cat] = {}
        return self.db['catalogs'][cat]

    def commit(self):
        self.sane()
        with open(self.path, 'w') as f:
            json.dump(self.db, f)


def load_from_json(path):
    return SimpleDb(path)


def test_db():
    db = load_from_json('db.json')
    gallery = db.get_catalog('galleries')
    gallery['summer'] = ['a.jpg', 'u.jpg']
    db.commit()

    with open('db.json') as f:
        pass
    assert json.load('db.json') == {
        'catalogs': {  #####
            'galleries': {  ####
                'summer': ['a.jpg', 'u.jpg']
            }
        }
    }